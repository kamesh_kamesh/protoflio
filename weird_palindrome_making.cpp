#include <iostream>
#include <bits/stdc++.h>
using namespace std;

int main(int argc, char const *argv[])
{
    int t;
    cin >> t;
    while (t--)
    {
        /* inputing Ai element */
        int n, odd_count=0;
        cin >> n;
        for(int i=0;i<n;i++)
        {
            int temp;
            cin >> temp;
            if(temp%2!=0) odd_count++;
        }

        if(odd_count%2!=0) odd_count--;
        cout<<odd_count/2 << endl;

    }
    
    return 0;
}
