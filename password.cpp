#include <bits/stdc++.h>
#include <cctype>
using namespace std;

bool isStrongPassword(string);
/******* Strong password program *******/

//abc123
int main () {
   string password;

   cout << "Enter password: ";
   getline (cin,password);

   if(isStrongPassword(password)) {

   	cout << "Strog password" << endl;
   }
   else 
   {
   	cout << "Not a strong password" << endl;
   }
   	
   return 0;
}

bool isStrongPassword(string s) {
	bool containsUpper = false,containSpecialChar = false, containNumber = false;
	
	for (auto c : s) //range for loop
	{
		if (isupper(c))
			containsUpper = true;

		if (ispunct(c))
			containSpecialChar = true;

		if(isdigit(c))
			containNumber = true;
	}

	if (containsUpper && containSpecialChar && containNumber && s.size()>=8)
		return true;

	return false;
}