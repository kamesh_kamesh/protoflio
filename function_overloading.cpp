#include <bits/stdc++.h>
using namespace std;

int volume(int s) {
    return s*s*s;
}

double volume(double r,int h) {
    return 3.14*r*r*static_cast<double>(h);
}

long volume(long l,int b,int h) {
    return l*b*h;
}

int main() {
    cout << volume(10) << endl;
    cout << volume(2.5,8) << endl;
    cout << volume(100l,75,15) << endl;
}