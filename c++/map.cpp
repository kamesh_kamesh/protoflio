//TOPIC : Map in C++

//NOTES :
//1.Synatax : map<T1,T2>obj; //Where T1 is key type and T2 is value type;
//2.std::map is associative container that store elements in key value combination.
//  Where key should be unique,otherwise it overrides the previous value.
//3.It is implement using self-balance binary search tree (AVL/Red Black Tree).
//4.It is store key value pair in stored order on the basis of key (assending/decending)
//5. std::map is generally used in dictionary type problems.

//EXAMPLE : Dictionary

#include <bits/stdc++.h>
#include <map>
using namespace std;

int main() {
    map<string, int, greater<string>> map;
    map["kamesh"] = 822040;
    map["akash"] = 909238;
    map.insert(make_pair("bot",809846));

    //loop through map
    for (auto &el1: map) 
    {
        cout << el1.first << " " << el1.second << endl;
    }

    // cout << map["kamesh"] << endl;
    return 0;
}
