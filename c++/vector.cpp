/****   vector introduction     ****/
#include <bits/stdc++.h>
using namespace std;

int main() {
    vector<int> v1 = {1,2,3,4,5};
    v1.push_back(6);
    for(auto i:v1) {
        cout << i;
    }
}