#include <bits/stdc++.h>
using namespace std;

class Room
{
    private:
        int length;
        int breadth;
    public:
        Room(int l,int b) {
            length = l;
            breadth= b;
        }
        ~Room () {
            cout << "Destractor called " << endl ;
        }
        int calculateArea() {
            return length*breadth;
        }
};

int main() {
    // creating a object in r1
    Room r1(15,10);
    Room r2(10,10);
    cout << r1.calculateArea() << endl;
    cout << r2.calculateArea() << endl;
    return 0;
}