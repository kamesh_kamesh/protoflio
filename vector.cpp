#include <bits/stdc++.h>
using namespace std;

/*----------------- using vectors -------------------*/

int main() {
    vector<int> v = {1,3,4,5,6,8,95,4};
    
    auto b = v.begin();
    while(b != v.end()) {
        cout << *b << " ";
        b++;
    } 
return 0;
}