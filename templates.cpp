#include <bits/stdc++.h>
using namespace std;

// one function works for all data types. This would work
// even for user defined types if operator '>' is overloaded
template <typename T>
T myMax (T x,T y) {
    return (x > y) ? x: y;
}

int main() {
    cout << myMax<int>(3,7) << endl; // call myMax for int 
    cout << myMax<double>(3.0,7.0) << endl; // call myMax for double
    cout << myMax<char>('g','c') << endl; // call myMax for char

    return 0;
}