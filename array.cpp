#include <iostream>
#include <bits/stdc++.h>
using namespace std;

int factorial(int num) {
    cout << "step:" << num << endl;
    if(num == 1) {
        return 1;
    }
    return num * factorial(num - 1);
}

int main(int argc, char const *argv[])
{
    cout << factorial(10) << endl;
}
